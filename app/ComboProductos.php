<?php

namespace AgenciaAdmin;

use Illuminate\Database\Eloquent\Model;
use DB;
class ComboProductos extends Model
{
    protected $table = 'combo_productos';
    protected $fillable = ['bebidas_id','productos_extra_id'];

    public static function getAll(){
    	return DB::table('combo_productos')
    			   ->join('bebidas','combo_productos.bebidas_id','=','bebidas.id')
                   ->join('productos_extra','combo_productos.productos_extra_id','=','productos_extra.id')
    			   ->select('combo_productos.*','bebidas.nombre as bebida_nombre','bebidas.precio_venta as bebida_p_venta',
    			   			'bebidas.precio_compra as bebida_p_compra','productos_extra.nombre as extra_nombre',
    			   			'productos_extra.precio_venta as extra_p_venta',
    			   			'productos_extra.precio_compra as extra_p_compra')
    			   ->paginate(5);
    }
}
