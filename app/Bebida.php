<?php

namespace AgenciaAdmin;

use Illuminate\Database\Eloquent\Model;
use DB;
class Bebida extends Model
{
    protected $table = 'bebidas';
    protected $fillable = ['nombre','precio_venta','precio_compra','ganancia','proveedores_id','tipos_bebida_id'];

    public static function getAll(){
    	return DB::table('bebidas')
    			   ->join('proveedores','bebidas.proveedores_id','=','proveedores.id')
                   ->join('tipos_bebida','bebidas.tipos_bebida_id','=','tipos_bebida.id')
    			   ->select('bebidas.*','proveedores.nombre_proveedor','tipos_bebida.tipo')
    			   ->paginate(5);
    }
}
