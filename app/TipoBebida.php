<?php

namespace AgenciaAdmin;
use DB;
use Illuminate\Database\Eloquent\Model;

class TipoBebida extends Model
{
    protected $table = 'tipos_bebida';
    protected $fillable = ['tipo','observaciones','created_at','updated_at'];

    public static function getAll(){
    	return DB::table('tipos_bebida')
    			   ->select('id','tipo','observaciones','created_at','updated_at','created_at')
    			   ->paginate(5);
    }
}
