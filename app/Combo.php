<?php

namespace AgenciaAdmin;

use Illuminate\Database\Eloquent\Model;
use DB;
class Combo extends Model
{
    protected $table = 'combos';
    protected $fillable = ['nombre_combo','descripcion','precio_venta','ganancia','bebidas_id','productos_extra_id'];

    public static function getAll(){
    	return DB::table('combos')
    			   ->select('combos.*')
    			   ->paginate(5);
    }
}
