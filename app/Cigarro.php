<?php

namespace AgenciaAdmin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Cigarro extends Model
{
    protected $table = 'cigarros';
    protected $fillable = ['marca','precio_compra','precio_venta_caja','precio_venta_unidad','proveedores_id'];

    public static function getAll_cigarros(){
    	return DB::table('cigarros')
    			   ->select('cigarros.*')
    			   ->paginate(10);
    }
    public static function getAll(){
    	return DB::table('cigarros')
    			   ->join('proveedores','cigarros.proveedores_id','=','proveedores.id')
    			   ->select('cigarros.*','proveedores.nombre_proveedor')
    			   ->paginate(10);
    }
}
