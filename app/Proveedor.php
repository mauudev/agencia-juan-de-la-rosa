<?php

namespace AgenciaAdmin;

use Illuminate\Database\Eloquent\Model;
use DB;
class Proveedor extends Model
{
    protected $table = 'proveedores';
    protected $fillable = ['nombre_proveedor','telefono_proveedor','nombre_empresa','telefono_empresa'];

    public static function getAll(){
    	return DB::table('proveedores')
    			   ->select('id','nombre_proveedor','telefono_proveedor','nombre_empresa','telefono_empresa','created_at')
    			   ->paginate(5);
    }
}
