<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::resource('admin','BebidasController');
Route::resource('proveedores','ProveedoresController');
Route::get('proveedores/{proveedores}/destroy',[
  'uses'=>'ProveedoresController@destroy',
  'as' => 'proveedores.destroy'
]);
Route::resource('bebidas','BebidasController');
Route::get('bebidas/{bebidas}/destroy',[
  'uses'=>'BebidasController@destroy',
  'as' => 'bebidas.destroy'
]);
Route::resource('extras','ProductosExtraController');
Route::get('extras/{extras}/destroy',[
  'uses'=>'ProductosExtraController@destroy',
  'as' => 'extras.destroy'
]);
Route::resource('tipos','TiposBebidaController');
Route::get('tipos/{tipos}/destroy',[
  'uses'=>'TiposBebidaController@destroy',
  'as' => 'tipos.destroy'
]);
Route::resource('combos','CombosController');
Route::get('combos/{combos}/destroy',[
  'uses'=>'CombosController@destroy',
  'as' => 'combos.destroy'
]);
Route::resource('categorias','CategoriasController');
Route::get('categorias/{categorias}/destroy',[
  'uses'=>'CategoriasController@destroy',
  'as' => 'categorias.destroy'
]);
Route::resource('cigarros','CigarrosController');
Route::get('cigarros/{cigarros}/destroy',[
  'uses'=>'CigarrosController@destroy',
  'as' => 'cigarros.destroy'
]);
Route::resource('combo-bebidas','ComboBebidasController');

Route::resource('combo-extras','ComboExtrasController');

Route::get('bebidas/icons',[
  'uses'=>'BebidasController@icons',
  'as' => 'bebidas.icons'
]);
