<?php

namespace AgenciaAdmin\Http\Controllers;

use Illuminate\Http\Request;

use AgenciaAdmin\Http\Requests;
use AgenciaAdmin\Cigarro;
use AgenciaAdmin\Proveedor;
use Session;
use DB;

class CigarrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cigarros = Cigarro::getAll();
        return view('cigarros.index',compact('cigarros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proveedores = Proveedor::lists('nombre_proveedor','id');
        return view('cigarros.create',compact('proveedores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cigarro::create($request->all());
        Session::flash('store-success','Datos agregados correctamente !');
        return redirect()->route('cigarros.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cigarro = Cigarro::find($id);
        $proveedores = Proveedor::lists('nombre_proveedor','id');
        return view('cigarros.edit',['cigarro'=>$cigarro,'proveedores'=>$proveedores_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cigarro = Cigarro::find($id);
        $cigarro->marca = $request->marca;
        $cigarro->precio_compra = $request->precio_compra;
        $cigarro->precio_venta_caja = $request->precio_venta_caja;
        $cigarro->precio_venta_unidad = $request->precio_venta_unidad;
        $cigarro->proveedores_id = $request->proveedores_id;
        $cigarro->save();
        Session::flash('update-success','Datos actualizados correctamente !');
        return redirect()->route('cigarros.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cigarro = Cigarro::find($id);
        $cigarro->delete();
        Session::flash('delete-success','Datos eliminados correctamente !');
        return redirect()->route('cigarros.index');
    }
}
