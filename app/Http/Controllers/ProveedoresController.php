<?php

namespace AgenciaAdmin\Http\Controllers;

use Illuminate\Http\Request;
use AgenciaAdmin\Proveedor;
use AgenciaAdmin\Http\Requests;
use Session;
class ProveedoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proveedores = Proveedor::getAll();
        return view('proveedores.index',['proveedores'=>$proveedores]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('proveedores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Proveedor::create($request->all());
        Session::flash("store-success","datos registrados correctamente !");
        return redirect()->route('proveedores.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proveedor = Proveedor::find($id);
        return view('proveedores.edit',['proveedor'=>$proveedor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $proveedor = Proveedor::find($id);
        $proveedor->nombre_proveedor = $request->nombre_proveedor;
        $proveedor->telefono_proveedor = $request->telefono_proveedor;
        $proveedor->nombre_empresa = $request->nombre_empresa;
        $proveedor->telefono_empresa = $request->telefono_empresa;
        $proveedor->save();
        Session::flash('update-success',"datos actualizados correctamente !");
        return redirect()->route('proveedores.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proveedor = Proveedor::find($id);
        $proveedor->delete();
        Session::flash('delete-success',"datos eliminados correctamente !");
        return redirect()->route('proveedores.index');
    }
}
