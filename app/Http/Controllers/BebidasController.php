<?php

namespace AgenciaAdmin\Http\Controllers;

use Illuminate\Http\Request;
use AgenciaAdmin\Proveedor;
use AgenciaAdmin\Bebida;
use AgenciaAdmin\TipoBebida;
use AgenciaAdmin\Http\Requests;
use Session;

class BebidasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bebidas = Bebida::getAll();
        //dd($bebidas->all());
        return view('bebidas.index',compact('bebidas'));
    }
    public function icons(){
        return view('agencia.icons');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proveedores = Proveedor::lists('nombre_proveedor','id');
        $tipos = TipoBebida::lists('tipo','id');
        return view('bebidas.create',compact('proveedores'),compact('tipos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bebida = new Bebida();
        $bebida->nombre = $request->nombre;
        $bebida->precio_compra = $request->precio_compra;
        $bebida->precio_venta = $request->precio_venta;
        $bebida->ganancia = ($request->precio_venta)-($request->precio_compra);
        $bebida->proveedores_id = $request->proveedores_id;
        $bebida->tipos_bebida_id = $request->tipos_bebida_id;
        $bebida->created_at = date("Y-m-d H:i:s");
        $bebida->updated_at = date("Y-m-d H:i:s");
        $bebida->save();
        Session::flash('store-success','Datos agregados correctamente !');
        return redirect()->route('bebidas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bebida = Bebida::find($id);
        $proveedores = Proveedor::lists('nombre_proveedor','id');
        $tipos = TipoBebida::lists('tipo','id');
        //dd($proveedores);
        return view('bebidas.edit',compact('proveedores'),['bebida'=>$bebida,'tipos'=>$tipos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bebida = Bebida::find($id);
        //dd($request->all());
        $bebida->nombre = $request->nombre;
        $bebida->precio_compra = $request->precio_compra;
        $bebida->precio_venta = $request->precio_venta;
        $bebida->proveedores_id = $request->proveedores_id;
        $bebida->tipos_bebida_id = $request->tipos_bebida_id;
        $bebida->save();
        Session::flash('update-success','Datos actualizados correctamente !');
        return redirect()->route('bebidas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bebida = Bebida::find($id);
        $bebida->delete();
        Session::flash('delete-success',"datos eliminados correctamente !");
        return redirect()->route('bebidas.index');
    }
}
