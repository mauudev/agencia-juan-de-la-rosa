<?php

namespace AgenciaAdmin\Http\Controllers;

use Illuminate\Http\Request;
use AgenciaAdmin\TipoBebida;
use AgenciaAdmin\Http\Requests;
use Session;
class TiposBebidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipos = TipoBebida::getAll();
        return view('tiposbebida.index',compact('tipos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tiposbebida.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        TipoBebida::create($request->all());
        Session::flash('store-success','Datos registrados correctamente !');
        return redirect()->route('tipos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipo = TipoBebida::find($id);
        return view('tiposbebida.edit',['tipo'=>$tipo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tipo = TipoBebida::find($id);
        $tipo->tipo = $request->tipo;
        $tipo->observaciones = $request->observaciones;
        $tipo->save();
        Session::flash('update-success','Datos actualizados correctamente !');
        return redirect()->route('tipos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipo = TipoBebida::find($id);
        $tipo->delete();
        Session::flash('delete-success','Datos eliminados correctamente !');
        return redirect()->route('tipos.index');
    }
}
