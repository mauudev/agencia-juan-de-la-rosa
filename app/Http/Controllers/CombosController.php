<?php
namespace AgenciaAdmin\Http\Controllers;

use Illuminate\Http\Request;
use AgenciaAdmin\Http\Requests;
use Session;
use DB;
use AgenciaAdmin\Combo;
use AgenciaAdmin\Bebida;
use AgenciaAdmin\ProductosExtra;
use AgenciaAdmin\ComboBebidas;
use AgenciaAdmin\ComboExtras;

class CombosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $combos = Combo::getAll();
        return view('combos.index',compact('combos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bebidas = Bebida::where('tipos_bebida_id','<>',7)->lists('nombre','id');//tragos
        $refrescos = Bebida::where('tipos_bebida_id','=',7)->lists('nombre','id');//refrescos
        $extras = ProductosExtra::where('categorias_id','=',7)->lists('nombre','id');//hielo
        $vista = "create";
        return view('combos.create',['vista'=>$vista,'bebidas'=>$bebidas,
                                     'extras'=>$extras,
                                     'refrescos'=>$refrescos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $nombre_combo = $request->nombre_combo;
        $precio_venta = $request->precio_venta;
        $precio_real = 0;
        $ganancia = 0;
        $descripcion = '';

        for ($i=0; $i < count($request->bebidas) ; $i++) { 
            $bebida = Bebida::find($request->bebidas[$i]);
            $precio_real += $bebida->precio_venta;
            $ganancia += $bebida->ganancia;
            $descripcion .= $bebida->nombre.", ";
        }
        for ($i=0; $i < count($request->refrescos) ; $i++) { 
            $bebida = Bebida::find($request->refrescos[$i]);
            $precio_real += $bebida->precio_venta;
            $ganancia += $bebida->ganancia;
            $descripcion .= $bebida->nombre.", ";
        }
        for ($i=0; $i < count($request->extras) ; $i++) { 
            $extra = ProductosExtra::find($request->extras[$i]);
            $precio_real += $extra->precio_venta;
            $ganancia += $extra->ganancia;
            $descripcion .= $extra->nombre.", ";
        }
        $descripcion = rtrim($descripcion, ", ");
        if('' == $descripcion) $descripcion = "Sin productos"; 
        $combo = new Combo();
        $combo->nombre_combo = $nombre_combo;
        $combo->descripcion = $descripcion;
        $combo->precio_venta = $precio_venta;
        $combo->precio_real = $precio_real;
        $combo->ganancia = $ganancia;
        $combo->save();
        // ULTIMO REGISTRO Esto me regresa el campo como un arreglo y puedo accederlo como un string ->first()
        $ult_registro = DB::table('combos')->select('id')
                                    ->orderBy('created_at', 'desc')
                                    ->take('1')
                                    ->first();
        // OK
        for ($i=0; $i < count($request->bebidas) ; $i++) { 
            $combo_bebidas = new ComboBebidas();
            $bebida = Bebida::find($request->bebidas[$i]);
            $combo_bebidas->bebidas_id = $bebida->id;
            $combo_bebidas->combos_id = $ult_registro->id;
            $combo_bebidas->save();
        }
        for ($i=0; $i < count($request->refrescos) ; $i++) { 
            $combo_bebidas = new ComboBebidas();
            $bebida = Bebida::find($request->refrescos[$i]);
            $combo_bebidas->bebidas_id = $bebida->id;
            $combo_bebidas->combos_id = $ult_registro->id;
            $combo_bebidas->save();
        }
        for ($i=0; $i < count($request->extras) ; $i++) { 
            $combo_producto_extra = new ComboExtras();
            $extra = ProductosExtra::find($request->extras[$i]);
            $combo_producto_extra->productos_extra_id = $extra->id;
            $combo_producto_extra->combos_id = $ult_registro->id;
            $combo_producto_extra->save();
        }
        Session::flash('store-success','Datos agregados correctamente !');
        return redirect()->route('combos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $combo_bebida = ComboBebidas::getTragos_combo($id);
        //dd($combo_bebida);
        $combo_extras = ComboExtras::getExtras_combo($id);
        //dd($combo_extras);
        $combo_refrescos = ComboBebidas::getRefrescos_combo($id);
        //dd($combo_refrescos);
        $combo = Combo::find($id);
        $bebidas = Bebida::where('tipos_bebida_id','<>',7)->lists('nombre','id');
        $extras = ProductosExtra::lists('nombre','id');
        $refrescos = Bebida::where('tipos_bebida_id','=',7)->lists('nombre','id');
        $vista = "edit";//para el js
        $tam_b = count($combo_bebida);
        $tam_e = count($combo_extras);
        $tam_r = count($combo_refrescos);
        return view('combos.edit',['bebidas'=>$bebidas,'refrescos'=>$refrescos,'extras'=>$extras,
                                   'c_bebidas'=>$combo_bebida,'c_refrescos'=>$combo_refrescos, 
                                   'c_extras'=>$combo_extras,'combo'=>$combo,'vista'=>$vista,
                                   'tam_b'=>$tam_b,'tam_e'=>$tam_e,'tam_r'=>$tam_r]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        //update a campos de combo
        $precio_real_new = 0;
        $ganancia = 0;
        $descripcion_new = '';
        //dd($request->bebidas);
        //update unicamente a campos viejos bebidas y extras
        if(isset($request->bebidas_old_ids)){
            for ($i=0; $i < count($request->combo_b_ids) ; $i++) { 
                $bebida = Bebida::find($request->bebidas_old_ids[$i]);
                $precio_real_new += $bebida->precio_venta;
                $ganancia += $bebida->ganancia;
                $descripcion_new .= $bebida->nombre.", "; 
                $combo_bebida = ComboBebidas::find($request->combo_b_ids[$i]);
                $combo_bebida->bebidas_id = $request->bebidas_old_ids[$i];
                $combo_bebida->save();
            }
        }
        if(isset($request->extras_old_ids)){
            for ($i=0; $i < count($request->combo_e_ids) ; $i++) { 
                $extra = ProductosExtra::find($request->extras_old_ids[$i]);
                $precio_real_new += $extra->precio_venta;
                $ganancia += $extra->ganancia;
                $descripcion_new .= $extra->nombre.", "; 
                $combo_extra = ComboExtras::find($request->combo_e_ids[$i]);
                $combo_extra->productos_extra_id = $request->extras_old_ids[$i];
                $combo_extra->save();
            }
        }
        //update a campos nuevos bebidas y extras
        if(isset($request->bebidas)){
            for($i = 0; $i < count($request->bebidas); $i++){
                $bebida = Bebida::find($request->bebidas[$i]);
                $precio_real_new += $bebida->precio_venta;
                $ganancia += $bebida->ganancia;
                $descripcion_new .= $bebida->nombre.", "; 
                $combo_bebida = new ComboBebidas();
                $combo_bebida->bebidas_id = $request->bebidas[$i];
                $combo_bebida->combos_id = $id;
                $combo_bebida->created_at = date("Y-m-d H:i:s");
                $combo_bebida->updated_at = date("Y-m-d H:i:s");
                $combo_bebida->save();
            }
        }
        if(isset($request->extras)){
            for($i = 0; $i < count($request->extras); $i++){
                $extra = ProductosExtra::find($request->extras[$i]);
                $precio_real_new += $extra->precio_venta;
                $ganancia += $extra->ganancia;
                $descripcion_new .= $extra->nombre.", "; 
                $combo_extras = new ComboExtras();
                $combo_extras->productos_extra_id = $request->extras[$i];
                $combo_extras->combos_id = $id;
                $combo_extras->created_at = date("Y-m-d H:i:s");
                $combo_extras->updated_at = date("Y-m-d H:i:s");
                $combo_extras->save();
            }
        }
        if('' == $descripcion_new)
            $descripcion_new = "Sin productos";
        else
            $descripcion_new = rtrim($descripcion_new, ", ");
        $combo = Combo::find($id);
        $combo->nombre_combo = $request->nombre_combo;
        $combo->descripcion = $descripcion_new;
        $combo->precio_venta = $request->precio_venta;
        $combo->precio_real = $precio_real_new;
        $combo->ganancia = $ganancia;
        $combo->save();
        Session::flash('update-success','Datos actualizados correctamente !');
        return redirect()->route('combos.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $combo = Combo::find($id);
        $combo->delete();
        Session::flash('delete-success','Datos eliminados correctamente !');
        return redirect()->route('combos.index');
    }
}
