<?php

namespace AgenciaAdmin\Http\Controllers;

use Illuminate\Http\Request;
use AgenciaAdmin\Proveedor;
use AgenciaAdmin\Http\Requests;
use AgenciaAdmin\ProductosExtra;
use AgenciaAdmin\Categoria;
use Session;
use DB;

class ProductosExtraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $extras = ProductosExtra::getAll();
        return view('productosextra.index',compact('extras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proveedores = Proveedor::lists('nombre_proveedor','id');
        $categorias = Categoria::lists('categoria','id');
        return view('productosextra.create',compact('proveedores'),compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $extra = new ProductosExtra();
        $extra->nombre = $request->nombre;
        $extra->precio_compra = $request->precio_compra;
        $extra->precio_venta = $request->precio_venta;
        $extra->ganancia = ($request->precio_venta)-($request->precio_compra);
        $extra->proveedores_id = $request->proveedores_id;
        $extra->categorias_id = $request->categorias_id;
        $extra->created_at = date("Y-m-d H:i:s");
        $extra->updated_at = date("Y-m-d H:i:s");
        $extra->save();
        Session::flash('store-success','Datos registrados correctamente !');
        return redirect()->route('extras.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $extra = ProductosExtra::find($id);
        $proveedores = Proveedor::lists('nombre_proveedor','id');
        $categorias = Categoria::lists('categoria','id');
        return view('productosextra.edit',['extra'=>$extra,'proveedores'=>$proveedores,'categorias'=>$categorias]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $extra = ProductosExtra::find($id);
        $extra->nombre = $request->nombre;
        $extra->precio_venta = $request->precio_venta;
        $extra->precio_compra = $request->precio_compra;
        $extra->proveedores_id = $request->proveedores_id;
        $extra->categorias_id = $request->categorias_id;
        $extra->save();
        Session::flash('update-success','Datos actualizados correctamente !');
        return redirect()->route('extras.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $extra = ProductosExtra::find($id);
        $extra->delete();
        Session::flash('delete-success','Datos eliminados correctamente !');
        return redirect()->route('extras.index');
    }
}
