<?php

namespace AgenciaAdmin;

use Illuminate\Database\Eloquent\Model;
use DB;
class ComboExtras extends Model
{
    protected $table = 'combo_extras';
    protected $fillable = ['productos_extra_id','combos_id'];

    public static function getExtras_combo($id){
      return DB::select("SELECT  p.nombre, ce.id as combo_e_id, p.id as extra_id
               			 FROM combo_extras ce, combos c, productos_extra p
                 		 WHERE ce.productos_extra_id = p.id AND ce.combos_id = c.id AND ce.combos_id = ".$id."
                 		 ORDER BY combo_e_id ASC");        
  }
}
