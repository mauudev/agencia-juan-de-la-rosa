<?php

namespace AgenciaAdmin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Categoria extends Model
{
    protected $table = 'categorias';
    protected $fillable = ['categoria'];

    public static function getAll_categorias(){
    	return DB::table('categorias')
    			   ->select('categorias.*')
    			   ->paginate(10);
    }
    public static function getCategoria($cat){
    	return Bebida::where('categoria','like',$cat.'%')->lists('categoria','id');		   
    }
}
