<?php

namespace AgenciaAdmin;

use Illuminate\Database\Eloquent\Model;
use DB;
class ComboBebidas extends Model
{
    protected $table = 'combo_bebidas';
    protected $fillable = ['bebidas_id','combos_id'];

  //   public static function getBebidas_combo($id){
  //     return DB::select("SELECT  cb.id, b.nombre as nombre_bebida, b.precio_venta as bebida_p_venta, 
  //     							 b.precio_compra as bebida_p_compra, c.nombre_combo, c.descripcion, c.precio_venta as combo_p_venta
  //              			 FROM combo_bebidas cb, combos c, bebidas b
  //                		 WHERE cb.bebidas_id = b.id AND cb.combos_id = c.id AND cb.combos_id = ".$id);        
  // }
    public static function getTragos_combo($id){
      return DB::select("SELECT  b.nombre,cb.id as combo_b_id, b.id as bebida_id 
                         FROM combo_bebidas cb, combos c, bebidas b, tipos_bebida t
                         WHERE t.id = b.tipos_bebida_id AND cb.bebidas_id = b.id AND cb.combos_id = c.id AND cb.combos_id = ".$id." AND t.id <> 7
                         ORDER BY combo_b_id ASC");        
    }
    public static function getRefrescos_combo($id){
      return DB::select("SELECT b.nombre,cb.id as combo_b_id, b.id as bebida_id 
                         FROM combo_bebidas cb, combos c, bebidas b, tipos_bebida t 
                         WHERE t.id = b.tipos_bebida_id AND cb.bebidas_id = b.id AND cb.combos_id = c.id AND cb.combos_id = ".$id." AND t.id = 7 
                         ORDER BY combo_b_id ASC");        
    }
    public static function getBebidas_combo1($id){
    	return DB::table('combo_bebidas')
    			   ->join('bebidas','combo_bebidas.bebidas_id','=','bebidas.id')
                   ->join('combos','combo_bebidas.combos_id','=','combos.id')
    			   ->select('bebidas.nombre','bebidas.id')
    			   ->where('combo_bebidas.id','=',$id)
    			   ->get();
    }
}
