<?php

namespace AgenciaAdmin;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductosExtra extends Model
{
    protected $table = 'productos_extra';
    protected $fillable = ['nombre','precio_venta','precio_compra','ganancia','proveedores_id','categorias_id'];

    public static function getAll(){
    	return DB::table('productos_extra')
    			   ->join('proveedores','productos_extra.proveedores_id','=','proveedores.id')
                   ->join('categorias','productos_extra.categorias_id','=','categorias.id')
    			   ->select('productos_extra.*','proveedores.nombre_proveedor','categorias.categoria')
    			   ->paginate(5);
    }
    public static function getProveedor_nombre(){
    	return DB::table('productos_extra')
    			   ->select('id','nombre','precio_venta','precio_compra','proveedores_id','created_at')
    			   ->paginate(5);
    }
    public static function getProveedor_detalles($id){
  	    return DB::select('SELECT id, nombre_proveedor, created_at, updated_at
                      FROM proveedores
                      WHERE id = '.$id)->first();
    }
}
