<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventarioExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventario_extras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cantidad_actual');
            $table->integer('cantidad_compra');
            $table->integer('productos_extra_id')->unsigned();
            $table->foreign('productos_extra_id')->references('id')->on('productos_extra');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inventario_extras');
    }
}
