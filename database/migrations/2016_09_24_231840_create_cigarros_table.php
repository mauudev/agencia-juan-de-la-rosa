<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCigarrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cigarros', function (Blueprint $table) {
            $table->increments('id');
            $table->string('marca');
            $table->decimal('precio_compra', 5, 2);
            $table->decimal('precio_venta_caja', 5, 2);
            $table->string('precio_venta_unidad'); 
            $table->integer('proveedores_id')->unsigned();
            $table->foreign('proveedores_id')->references('id')->on('proveedores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cigarros');
    }
}
