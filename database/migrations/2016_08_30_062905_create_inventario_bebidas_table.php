<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventarioBebidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventario_bebidas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cantidad_actual');
            $table->integer('cantidad_compra')->nullable();
            $table->integer('bebidas_id')->unsigned();
            $table->foreign('bebidas_id')->references('id')->on('bebidas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inventario_bebidas');
    }
}
