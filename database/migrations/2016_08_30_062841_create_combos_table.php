<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCombosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('combos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_combo');
            $table->string('descripcion')->nullable()->default('Sin especificar');
            $table->decimal('precio_venta', 5, 2);
            $table->decimal('precio_real', 5, 2); 
            $table->decimal('ganancia', 5, 2);  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('combos');
    }
}
