<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComboBebidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('combo_bebidas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bebidas_id')->unsigned();
            $table->foreign('bebidas_id')->references('id')->on('bebidas');
            $table->integer('combos_id')->unsigned();
            $table->foreign('combos_id')->references('id')->on('combos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('combo_bebidas');
    }
}
