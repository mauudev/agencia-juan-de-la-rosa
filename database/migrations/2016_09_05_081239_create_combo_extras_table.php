<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComboExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('combo_extras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('productos_extra_id')->unsigned();
            $table->foreign('productos_extra_id')->references('id')->on('productos_extra');
            $table->integer('combos_id')->unsigned();
            $table->foreign('combos_id')->references('id')->on('combos')->onDelete('cascade');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('combo_extras');
    }
}
