<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBebidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bebidas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->decimal('precio_compra', 5, 2)->nullable();
            $table->decimal('precio_venta', 5, 2);
            $table->decimal('ganancia', 5, 2);
            $table->integer('proveedores_id')->unsigned();
            $table->foreign('proveedores_id')->references('id')->on('proveedores');
            $table->integer('tipos_bebida_id')->unsigned();
            $table->foreign('tipos_bebida_id')->references('id')->on('tipos_bebida');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bebidas');
    }
}
