<?php

use Illuminate\Database\Seeder;

class ProductosExtraTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('productos_extra')->delete();
        
        \DB::table('productos_extra')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Hielo "Amazonas"',
                'precio_compra' => '5.00',
                'precio_venta' => '6.00',
                'ganancia' => '1.00',
                'proveedores_id' => 2,
                'categorias_id' => 7,
                'created_at' => '2016-09-05 08:31:30',
                'updated_at' => '2016-09-05 08:31:30',
            ),
        ));
        
        
    }
}
