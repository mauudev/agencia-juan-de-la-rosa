<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categorias')->delete();
        
        \DB::table('categorias')->insert(array (
            0 => 
            array (
                'id' => 1,
                'categoria' => 'Cigarros',
                'created_at' => '2016-09-24 22:28:32',
                'updated_at' => '2016-09-24 22:28:32',
            ),
            1 => 
            array (
                'id' => 2,
                'categoria' => 'Saladitos',
                'created_at' => '2016-09-24 22:28:44',
                'updated_at' => '2016-09-24 22:28:44',
            ),
            2 => 
            array (
                'id' => 3,
                'categoria' => 'Dulces',
                'created_at' => '2016-09-24 22:28:51',
                'updated_at' => '2016-09-24 22:28:51',
            ),
            3 => 
            array (
                'id' => 4,
                'categoria' => 'Galletas',
                'created_at' => '2016-09-24 22:28:58',
                'updated_at' => '2016-09-24 22:28:58',
            ),
            4 => 
            array (
                'id' => 5,
                'categoria' => 'Papel',
                'created_at' => '2016-09-24 22:29:40',
                'updated_at' => '2016-09-24 22:29:40',
            ),
            5 => 
            array (
                'id' => 6,
                'categoria' => 'Pañales',
                'created_at' => '2016-09-24 22:29:53',
                'updated_at' => '2016-09-24 22:30:26',
            ),
            6 => 
            array (
                'id' => 7,
                'categoria' => 'Hielo',
                'created_at' => '2016-09-24 22:30:47',
                'updated_at' => '2016-09-24 22:31:37',
            ),
            7 => 
            array (
                'id' => 8,
                'categoria' => 'Otros',
                'created_at' => '2016-09-24 22:31:45',
                'updated_at' => '2016-09-24 22:31:45',
            ),
        ));
        
        
    }
}
