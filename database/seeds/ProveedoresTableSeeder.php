<?php

use Illuminate\Database\Seeder;

class ProveedoresTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('proveedores')->delete();
        
        \DB::table('proveedores')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre_proveedor' => 'Daniel Perez',
                'telefono_proveedor' => '78933255',
                'nombre_empresa' => 'Coca Cola',
                'telefono_empresa' => '4522255',
                'created_at' => '2016-09-05 06:10:45',
                'updated_at' => '2016-09-05 06:10:45',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre_proveedor' => 'Marco Antonio Mamani',
                'telefono_proveedor' => '78933255 - 79311454',
                'nombre_empresa' => 'Tigo',
                'telefono_empresa' => '4522255',
                'created_at' => '2016-09-05 06:10:52',
                'updated_at' => '2016-09-05 06:10:52',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre_proveedor' => 'Don Manuel',
                'telefono_proveedor' => '77474743',
                'nombre_empresa' => 'S/N',
                'telefono_empresa' => '4452525',
                'created_at' => '2016-09-05 06:11:06',
                'updated_at' => '2016-09-05 06:11:06',
            ),
        ));
        
        
    }
}
