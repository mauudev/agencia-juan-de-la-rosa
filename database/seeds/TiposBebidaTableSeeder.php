<?php

use Illuminate\Database\Seeder;

class TiposBebidaTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tipos_bebida')->delete();
        
        \DB::table('tipos_bebida')->insert(array (
            0 => 
            array (
                'id' => 1,
                'tipo' => 'Ron',
                'observaciones' => '',
                'created_at' => '2016-09-05 06:11:32',
                'updated_at' => '2016-09-05 06:11:32',
            ),
            1 => 
            array (
                'id' => 2,
                'tipo' => 'Fernet',
                'observaciones' => '',
                'created_at' => '2016-09-05 06:11:38',
                'updated_at' => '2016-09-05 06:11:38',
            ),
            2 => 
            array (
                'id' => 3,
                'tipo' => 'Vodka',
                'observaciones' => '',
                'created_at' => '2016-09-05 06:11:43',
                'updated_at' => '2016-09-05 06:11:43',
            ),
            3 => 
            array (
                'id' => 4,
                'tipo' => 'Cerveza',
                'observaciones' => '',
                'created_at' => '2016-09-05 06:11:49',
                'updated_at' => '2016-09-05 06:11:49',
            ),
            4 => 
            array (
                'id' => 5,
                'tipo' => 'Singani',
                'observaciones' => '',
                'created_at' => '2016-09-05 06:11:56',
                'updated_at' => '2016-09-05 06:11:56',
            ),
            5 => 
            array (
                'id' => 6,
                'tipo' => 'Whisky',
                'observaciones' => '',
                'created_at' => '2016-09-05 06:16:55',
                'updated_at' => '2016-09-05 06:16:55',
            ),
            6 => 
            array (
                'id' => 7,
                'tipo' => 'Refresco',
                'observaciones' => 'S/D',
                'created_at' => '2016-09-05 08:30:11',
                'updated_at' => '2016-09-05 08:30:42',
            ),
        ));
        
        
    }
}
