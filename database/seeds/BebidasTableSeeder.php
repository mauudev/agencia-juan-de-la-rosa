<?php

use Illuminate\Database\Seeder;

class BebidasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('bebidas')->delete();
        
        \DB::table('bebidas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Fernet Branca',
                'precio_compra' => '65.00',
                'precio_venta' => '70.00',
                'ganancia' => '5.00',
                'proveedores_id' => 3,
                'tipos_bebida_id' => 2,
                'created_at' => '2016-09-05 06:16:11',
                'updated_at' => '2016-09-05 06:36:43',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Ron Abuelo',
                'precio_compra' => '90.00',
                'precio_venta' => '105.00',
                'ganancia' => '15.00',
                'proveedores_id' => 3,
                'tipos_bebida_id' => 1,
                'created_at' => '2016-09-05 06:16:25',
                'updated_at' => '2016-09-05 06:16:25',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Johnnie Walker Rojo',
                'precio_compra' => '120.00',
                'precio_venta' => '145.00',
                'ganancia' => '25.00',
                'proveedores_id' => 2,
                'tipos_bebida_id' => 6,
                'created_at' => '2016-09-05 06:16:45',
                'updated_at' => '2016-09-05 06:36:53',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Coca-Cola',
                'precio_compra' => '10.00',
                'precio_venta' => '11.00',
                'ganancia' => '1.00',
                'proveedores_id' => 2,
                'tipos_bebida_id' => 7,
                'created_at' => '2016-09-05 08:31:03',
                'updated_at' => '2016-09-05 08:31:03',
            ),
        ));
        
        
    }
}
