<script type="text/javascript">
$(document).on('click','.btn-add-trago',function(e){
	e.preventDefault();
	var template = '<div class="form-group option-container"><div class="control-label col-md-2"></div><div class="col-md-5 pull-left" >{!! Form::select("bebidas[]", $bebidas,null,["id"=>"select1","class"=>"form-control","placeholder"=>"Seleccione un trago..","required"]) !!}</div><div class="col-md-2"><button class="btn btn-success btn-remove-b" onclick="deleteElementB()" data-tooltip="Quitar elemento"><b>X</b></button></div></div>';
    $("#begin1").after(template);
});
$(document).on('click','.btn-add-refresco',function(e){
  e.preventDefault();
  var template = '<div class="form-group option-container"><div class="control-label col-md-2"></div><div class="col-md-5 pull-left" >{!! Form::select("bebidas[]", $refrescos,null,["id"=>"select1","class"=>"form-control","placeholder"=>"Seleccione un refresco..","required"]) !!}</div><div class="col-md-2"><button class="btn btn-flickr btn-remove-b" onclick="deleteElementB()" data-tooltip="Quitar elemento"><b>X</b></button></div></div>';
    $("#begin1").after(template);
});
$(document).on('click','.btn-add-extra',function(e){
	e.preventDefault();
	var template = '<div class="form-group option-container"><div class="control-label col-md-2"></div><div class="col-md-5 pull-left" >{!! Form::select("extras[]", $extras,null,["id"=>"select2","class"=>"form-control","placeholder"=>"Seleccione un producto..","required"]) !!}</div><div class="col-md-2"><button class="btn btn-info btn-remove-e" onclick="deleteElementE()" data-tooltip="Quitar elemento"><b>X</b></button></div></div>';
    $("#begin1").after(template);
});

$(document).on('click','.btn-remove-b',function(e){
  var vista = {!!json_encode($vista)!!};
    if("edit" == vista){
      e.preventDefault();
      var id = $(this).parent('span').attr('opcion-id-b');
      var form = $('#form-delete-b');
      var url = form.attr('action').replace(':OPTION_ID_B',id);
      var data = form.serialize();  
      $.post(url, data, function(result){
          //alert(result);
        });
      $(this).parents('.option-container').remove();
  }
  if("create" == vista){
    e.preventDefault();
    $(this).parents('.option-container').remove();
  }
});
$(document).on('click','.btn-remove-e',function(e){
  var vista = {!!json_encode($vista)!!};
    if("edit" == vista){
      e.preventDefault();
      var id = $(this).parent('span').attr('opcion-id-e');
      var form = $('#form-delete-e');
      var url = form.attr('action').replace(':OPTION_ID_E',id);
      var data = form.serialize();  
      $.post(url, data, function(result){
          //alert(result);
        });
      $(this).parents('.option-container').remove();
  }
  if("create" == vista){
    e.preventDefault();
    $(this).parents('.option-container').remove();
  }
});


function deleteElementB(){
}
function deleteElementB(){
}
function deleteElementE(){
}


</script>
