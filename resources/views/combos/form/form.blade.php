<div class="form-group">
	{!! Form::label('nombre_combo','Nombre del combo: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!! Form::text('nombre_combo',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del combo','required','min'=>5]) !!}<br/>
	</div>
</div>
<div class="form-group">
	{!! Form::label('precio_venta','Precio de venta: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!! Form::text('precio_venta',null,['class'=>'form-control','placeholder'=>'Ingrese el precio de venta','required','min'=>5]) !!}<br/>
	</div>
</div>
<div class="form-group" id="begin1">
	{!! Form::label('bebida','Agregar productos: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		<button class="btn btn-success btn-add-trago"><i class="fa fa-xing"></i>Agregar tragos</button>
		<button class="btn btn-flickr btn-add-refresco"><i class="fa fa-xing"></i>Agregar refrescos</button>
		<button class="btn btn-info btn-add-extra" ><i class="fa fa-flickr"></i>Agregar extras</button>
	</div>
</div>
