@extends('agencia.principal')
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="widget-container fluid-height clearfix">
			<div class="heading">
				<i class="fa fa-bars"></i>Registrar nuevo combo {{ $vista }}
			</div>
			<div class="widget-content padded">
				{!! Form::open(['route'=>'combos.store','method'=>'POST','class'=>'form-horizontal']) !!}
				@include('combos.form.form')
				<div class="form-group">
					<div class="col-md-7" >
						{!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!} <a class="btn btn-default-outline" href="{!! URL::to('combos') !!}">Cancelar</a>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
@include('combos.combos-form')
@endsection