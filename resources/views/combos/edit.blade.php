@extends('agencia.principal')
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="widget-container fluid-height clearfix">
			<div class="heading">
				<i class="fa fa-bars"></i>Actualizar datos del combo
			</div>
			<div class="widget-content padded">
				{!!Form::model($combo,['route'=>['combos.update',$combo->id],'method'=>'PUT','class'=>'form-horizontal'])!!}
				{!! Form::hidden('combo_id',$combo->id) !!}
				@include('combos.form.form')
				@for($i = 0; $i < $tam_b; $i ++)<!-- population de las bebidas seleccionadas y todas las bebidas la clave esta en el modelo, fijarse los campos bebida_id y combo_id ORDER ASC -->
				<div class="form-group option-container">
					<div class="control-label col-md-2"></div>
						<div class="col-md-5 pull-left" >
							{!! Form::select("bebidas_old_ids[]", $bebidas,$c_bebidas[$i]->bebida_id,["id"=>"select1","class"=>"form-control","placeholder"=>"Seleccione una bebida..","required"]) !!}
							{!! Form::hidden('combo_b_ids[]',$c_bebidas[$i]->combo_b_id) !!}
						</div>
						<div class="col-md-2">
						<span opcion-id-b="{{$c_bebidas[$i]->combo_b_id}}" class="input-group-btn">
								<button class="btn btn-success btn-remove-b" onclick="return confirm('Esta seguro de eliminar? Esta opci&oacute;n no se puede deshacer !')" data-tooltip="Quitar elemento"><b>X</b>
								</button>
							</span>
						</div>
				</div>
				@endfor
				@for($i = 0; $i < $tam_r; $i ++)<!-- population de las bebidas seleccionadas y todas las bebidas la clave esta en el modelo, fijarse los campos bebida_id y combo_id ORDER ASC -->
				<div class="form-group option-container">
					<div class="control-label col-md-2"></div>
						<div class="col-md-5 pull-left" >
							{!! Form::select("bebidas_old_ids[]", $refrescos,$c_refrescos[$i]->bebida_id,["id"=>"select1","class"=>"form-control","placeholder"=>"Seleccione una bebida..","required"]) !!}
							{!! Form::hidden('combo_b_ids[]',$c_refrescos[$i]->combo_b_id) !!}
						</div>
						<div class="col-md-2">
						<span opcion-id-b="{{$c_refrescos[$i]->combo_b_id}}" class="input-group-btn">
								<button class="btn btn-info btn-remove-b" onclick="return confirm('Esta seguro de eliminar? Esta opci&oacute;n no se puede deshacer !')" data-tooltip="Quitar elemento"><b>X</b>
								</button>
							</span>
						</div>
				</div>
				@endfor
				@for($i = 0; $i < $tam_e; $i ++)<!-- population de las bebidas seleccionadas y todas las bebidas la clave esta en el modelo, fijarse los campos bebida_id y combo_id ORDER ASC -->
				<div class="form-group option-container">
					<div class="control-label col-md-2"></div>
						<div class="col-md-5 pull-left" >
							{!! Form::select("extras_old_ids[]", $extras,$c_extras[$i]->extra_id,["id"=>"select1","class"=>"form-control","placeholder"=>"Seleccione una bebida..","required"]) !!}
							{!! Form::hidden('combo_e_ids[]',$c_extras[$i]->combo_e_id) !!}
						</div>
						<div class="col-md-2">
						<span opcion-id-e="{{$c_extras[$i]->combo_e_id}}" class="input-group-btn">
								<button class="btn btn-flickr btn-remove-e" onclick="return confirm('Esta seguro de eliminar? Esta opci&oacute;n no se puede deshacer !')" data-tooltip="Quitar elemento"><b>X</b>
								</button>
							</span>
						</div>
				</div>
				@endfor
				<div class="form-group">
					<div class="col-md-7" >
						{!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!} <a class="btn btn-default-outline" href="{!! URL::to('combos') !!}">Cancelar</a>
					</div>
				</div>
				{!! Form::close() !!}
				
			</div>
		</div>
	</div>
</div>
{!!Form::open(['route'=>['combo-bebidas.destroy',':OPTION_ID_B'],'method'=>'DELETE','id'=>'form-delete-b'])!!}
{!!Form::close()!!}
{!!Form::open(['route'=>['combo-extras.destroy',':OPTION_ID_E'],'method'=>'DELETE','id'=>'form-delete-e'])!!}
{!!Form::close()!!}
@endsection
@section('scripts')
@include('combos.combos-form')
@endsection