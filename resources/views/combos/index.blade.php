@extends('agencia.principal')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="widget-container fluid-height clearfix">
      <div class="heading">
        <i class="fa fa-table"></i>Lista de combos
        <a class="btn btn-primary pull-right" data-style="expand-right" href="{!! URL::to('combos/create') !!}"><i class="fa fa-plus"></i>Agregar nuevo</a>
        &nbsp;<a class="btn btn-info pull-right" data-style="expand-right" href="{!! URL::to('combos') !!}"><i class="fa fa-refresh"></i>Actualizar</a>
      </div>
      <div class="widget-content padded clearfix">
        <div id="dataTable1_wrapper" class="dataTables_wrapper" role="grid">
          <div class="dataTables_filter" id="dataTable1_filter">
            <label>Buscar:
              <input type="text" aria-controls="dataTable1">
            </label>         
          </div>
          <table class="table table-bordered table-striped dataTable" aria-describedby="dataTable1_info">
            <thead>
              <tr role="row">
                <th class="sorting_asc" role="columnheader" tabindex="0" rowspan="1" colspan="1"  style="width: 183px;">
                  Nombre del combo
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Descripcion
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Precio venta con descuento
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Precio real sin descuento
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Fecha de creaci&oacute;n
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 50px;">
                  Acci&oacute;n
                </th>
              </tr>
            </thead>
            <tbody role="alert" aria-live="polite" aria-relevant="all">
              @foreach($combos as $combo)
              <tr class="odd">
                <td class=" sorting_1">
                  {{ $combo->nombre_combo }}
                </td>
                <td class="">
                  {{ $combo->descripcion }}
                </td>
                <td class="hidden-xs">
                  {{ $combo->precio_venta }} Bs
                </td>
                <td class="hidden-xs">
                  {{ $combo->precio_real }} Bs
                </td>
                <td class="hidden-xs">
                  {{ $combo->created_at }}
                </td>
                <td class="actions ">
                  <div class="action-buttons" align="center">
                    <a class="table-actions" data-toggle="modal" href="#myModal{{ $combo->id }}"><i class="fa fa-eye"></i></a>
                    <a class="table-actions" href="{!! route('combos.edit',$parameters = $combo->id) !!}">
                      <i class="fa fa-pencil"></i>
                    </a>
                    <a class="table-actions" href="{!! route('combos.destroy',$parameters = $combo->id) !!}" onclick="return confirm('Esta seguro de eliminar? Esta opci&oacute;n no se puede deshacer !')">
                      <i class="fa fa-trash-o"></i>
                    </a>
                  </div>
                  <div class="modal fade" id="myModal{{ $combo->id }}" aria-hidden="true" style="display: none;">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                        <h4 class="modal-title">
                          Detalles
                        </h4>
                      </div>
                      <div class="modal-body">
                        <h1>
                          Detalles del combo
                        </h1>
                        <p><b>Nombre del combo: </b></p>
                        <p>{{ $combo->nombre_combo }}</p>
                        <p><b>Descripci&oacute;n: </b></p>
                        <p> Este combo contiene los siguientes productos: {{ $combo->descripcion }}</p>
                        <p><b>Precio de venta </b></p>
                        <p>{{ $combo->precio_venta }} Bs</p>
                        <p><b>Precio real calculado </b></p>
                        <p>{{ $combo->precio_real }} Bs</p>
                        <p><b>Ganancia neta (sin descuento)</b></p>
                        <p>{{ $combo->ganancia }} Bs</p>
                        <p><b>Ganancia de la venta (con descuento) </b></p>
                        <p>{{ $combo->precio_venta - $combo->precio_real }} Bs</p>
                        <p><b>Fecha de creaci&oacute;n: </b></p>
                        <p>{{ $combo->created_at }}</p>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-default-outline" data-dismiss="modal" type="button">Cerrar</button>
                      </div>
                    </div>
                  </div>
                </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          {{ $combos->render() }}
          @include('alerts.success')
        </div>
      </div>
    </div>
  </div>
  @endsection