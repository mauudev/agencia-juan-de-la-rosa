@extends('agencia.principal')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="widget-container fluid-height clearfix">
      <div class="heading">
        <i class="fa fa-table"></i>Todos los tipos de bebida
        <a class="btn btn-primary pull-right" data-style="expand-right" href="{!! URL::to('tipos/create') !!}"><i class="fa fa-plus"></i>Agregar nuevo</a>
        &nbsp;<a class="btn btn-info pull-right" data-style="expand-right" href="{!! URL::to('tipos') !!}"><i class="fa fa-refresh"></i>Actualizar</a>
      </div>
      <div class="widget-content padded clearfix">
        <div id="dataTable1_wrapper" class="dataTables_wrapper" role="grid">
          <div class="dataTables_filter" id="dataTable1_filter">
            <label>Buscar:
              <input type="text" aria-controls="dataTable1">
            </label>         
          </div>
          <table class="table table-bordered table-striped dataTable" aria-describedby="dataTable1_info">
            <thead>
              <tr role="row">
                <th class="sorting_asc" role="columnheader" tabindex="0" rowspan="1" colspan="1"  style="width: 183px;">
                  Tipo de bebida
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Observaciones
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Fecha de creaci&oacute;n
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Fecha de modificaci&oacute;n
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 50px;">
                  Acci&oacute;n
                </th>
              </tr>
            </thead>
            <tbody role="alert" aria-live="polite" aria-relevant="all">
              @foreach($tipos as $tipo)
              <tr class="odd">
                <td class=" sorting_1">
                  {{ $tipo->tipo }}
                </td>
                <td class="">
                  {{ $tipo->observaciones }}
                </td>
                <td class="hidden-xs">
                  {{ $tipo->created_at }}
                </td>
                <td class="hidden-xs">
                  {{ $tipo->updated_at }}
                </td>
                <td class="actions ">
                  <div class="action-buttons" align="center">
                    <a class="table-actions" data-toggle="modal" href="#myModal{{ $tipo->id }}"><i class="fa fa-eye"></i></a>
                    <a class="table-actions" href="{!! route('tipos.edit',$parameters = $tipo->id) !!}">
                      <i class="fa fa-pencil"></i>
                    </a>
                    <a class="table-actions" href="{!! route('tipos.destroy',$parameters = $tipo->id) !!}">
                      <i class="fa fa-trash-o"></i>
                    </a>
                  </div>
                  <div class="modal fade" id="myModal{{ $tipo->id }}" aria-hidden="true" style="display: none;">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                        <h4 class="modal-title">
                          Detalles
                        </h4>
                      </div>
                      <div class="modal-body">
                        <h1>
                          Detalles del producto
                        </h1>
                        <p><b>Tipo de bebida: </b></p>
                        <p>{{ $tipo->tipo }}</p>
                        <p><b>Observaciones: </b></p>
                        <p>{{ $tipo->observaciones }}</p>
                        <p><b>Fecha de creaci&oacute;n</b></p>
                        <p>{{ $tipo->created_at }}</p>
                        <p><b>Fecha de modificaci&oacute;n: </b></p>
                        <p>{{ $tipo->updated_at }}</p>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-default-outline" data-dismiss="modal" type="button">Cerrar</button>
                      </div>
                    </div>
                  </div>
                </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          {{ $tipos->render() }}
          @include('alerts.success')
        </div>
      </div>
    </div>
  </div>
  @endsection