<div class="form-group">
	{!! Form::label('tipo','Tipo de bebida: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!! Form::text('tipo',null,['class'=>'form-control','placeholder'=>'Ingrese el tipo de bebida','required','min'=>5]) !!}<br/>
	</div>
</div>
<div class="form-group">
	{!! Form::label('observaciones','Observaciones: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!! Form::textarea('observaciones',null,['class'=>'form-control','placeholder'=>'Ingrese alguna observaci&oacute;n','min'=>10,'rows'=>4]) !!}<br/>
	</div>
</div>