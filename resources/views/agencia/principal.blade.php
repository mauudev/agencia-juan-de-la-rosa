<!DOCTYPE html>
<html>
  <head>
    <title>
      Agencia Juan de la Rosa | Administracion
    </title>
    {{-- <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" /> --}}
    {!! Html::style("stylesheets/bootstrap.min.css") !!}
    {!! Html::style('css/font-awesome.css') !!}
    {!! Html::style("stylesheets/hightop-font.css") !!}
    {!! Html::style("stylesheets/isotope.css") !!}
    {!! Html::style("stylesheets/jquery.fancybox.css") !!}
    {!! Html::style("stylesheets/fullcalendar.css") !!}
    {!! Html::style("stylesheets/wizard.css") !!}
    {!! Html::style("stylesheets/select2.css") !!}
    {!! Html::style("stylesheets/morris.css") !!}
    {!! Html::style("stylesheets/datatables.css") !!}
    {!! Html::style("stylesheets/datepicker.css") !!}
    {!! Html::style("stylesheets/timepicker.css") !!}
    {!! Html::style("stylesheets/colorpicker.css") !!}
    {!! Html::style("stylesheets/bootstrap-switch.css") !!}
    {!! Html::style("stylesheets/bootstrap-editable.css") !!}
    {!! Html::style("stylesheets/daterange-picker.css") !!}
    {!! Html::style("stylesheets/typeahead.css") !!}
    {!! Html::style("stylesheets/summernote.css") !!}
    {!! Html::style("stylesheets/ladda-themeless.min.css") !!}
    {!! Html::style("stylesheets/social-buttons.css") !!}
    {!! Html::style("stylesheets/jquery.fileupload-ui.css") !!}
    {!! Html::style("stylesheets/dropzone.css") !!}
    {!! Html::style("stylesheets/nestable.css") !!}
    {!! Html::style("stylesheets/pygments.css") !!}
    {!! Html::style("stylesheets/style.css") !!}
    {!! Html::style("stylesheets/color/green.css") !!}
    {!! Html::style("stylesheets/color/orange.css") !!}
    {!! Html::style("stylesheets/color/magenta.css") !!}
    {!! Html::style("stylesheets/color/gray.css") !!}
    {!! Html::style("css/tooltips.css") !!}

    {!!Html::script('js/jquery.min.js')!!}
{{--     {!! Html::script("http://code.jquery.com/ui/1.10.3/jquery-ui.js") !!} --}}
    {!! Html::script("javascripts/bootstrap.min.js") !!}
    {!! Html::script("javascripts/raphael.min.js") !!}
    {!! Html::script("javascripts/selectivizr-min.js") !!}
    {!! Html::script("javascripts/jquery.mousewheel.js") !!}
    {!! Html::script("javascripts/jquery.vmap.min.js") !!}
    {!! Html::script("javascripts/jquery.vmap.sampledata.js") !!}
    {!! Html::script("javascripts/jquery.vmap.world.js") !!}
    {!! Html::script("javascripts/jquery.bootstrap.wizard.js") !!}
    {!! Html::script("javascripts/fullcalendar.min.js") !!}
    {!! Html::script("javascripts/gcal.js") !!}
    {!! Html::script("javascripts/jquery.dataTables.min.js") !!}
    {!! Html::script("javascripts/jquery.easy-pie-chart.js") !!}
    {!! Html::script("javascripts/excanvas.min.js") !!}
    {!! Html::script("javascripts/jquery.isotope.min.js") !!}
    {{--{!! Html::script("javascripts/isotope_extras.js") !!}--}}
    {!! Html::script("javascripts/modernizr.custom.js") !!}
    {!! Html::script("javascripts/jquery.fancybox.pack.js") !!}
    {!! Html::script("javascripts/select2.js") !!}
    {!! Html::script("javascripts/styleswitcher.js") !!}
    {!! Html::script("javascripts/wysiwyg.js") !!}
    {!! Html::script("javascripts/typeahead.js") !!}
    {!! Html::script("javascripts/summernote.min.js") !!}
    {!! Html::script("javascripts/jquery.inputmask.min.js") !!}
    {!! Html::script("javascripts/jquery.validate.js") !!}
    {!! Html::script("javascripts/bootstrap-fileupload.js") !!}
    {!! Html::script("javascripts/bootstrap-datepicker.js") !!}
    {!! Html::script("javascripts/bootstrap-timepicker.js") !!}
    {!! Html::script("javascripts/bootstrap-colorpicker.js") !!}
    {!! Html::script("javascripts/bootstrap-switch.min.js") !!}
    {!! Html::script("javascripts/typeahead.js") !!}
    {!! Html::script("javascripts/spin.min.js") !!}
    {!! Html::script("javascripts/ladda.min.js") !!}
    {!! Html::script("javascripts/moment.js") !!}
    {!! Html::script("javascripts/mockjax.js") !!}
    {!! Html::script("javascripts/bootstrap-editable.min.js") !!}
    {{--{!! Html::script("javascripts/xeditable-demo-mock.js") !!}
    {!! Html::script("javascripts/xeditable-demo.js") !!}--}}
    {!! Html::script("javascripts/address.js") !!}
    {!! Html::script("javascripts/daterange-picker.js") !!}
    {!! Html::script("javascripts/date.js") !!}
    {!! Html::script("javascripts/morris.min.js") !!}
    {!! Html::script("javascripts/skycons.js") !!}
    {!! Html::script("javascripts/fitvids.js") !!}
    {!! Html::script("javascripts/jquery.sparkline.min.js") !!}
    {!! Html::script("javascripts/dropzone.js") !!}
    {!! Html::script("javascripts/jquery.nestable.js") !!}
    {{-- {!! Html::script("javascripts/main.js") !!}--}}
    {!! Html::script("javascripts/respond.js") !!}

    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
  </head>
  <body class="page-header-fixed bg-1">
    <div class="modal-shiftfix">
      <!-- Navigation -->
      <div class="navbar navbar-fixed-top scroll-hide">
        <div class="container-fluid top-bar">
          <div class="pull-right">
            <ul class="nav navbar-nav pull-right">
              <li class="dropdown user hidden-xs"><a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
                <img width="34" height="34" src="../../images/avatar-male.jpg" />John Smith<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="index.html#">
                    <i class="fa fa-user"></i>My Account</a>
                  </li>
                  <li><a href="index.html#">
                    <i class="fa fa-gear"></i>Account Settings</a>
                  </li>
                  <li><a href="login1.html">
                    <i class="fa fa-sign-out"></i>Logout</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <button class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="logo" href="index.html">Agencia Juan de la Rosa</a>
        </div>
        <div class="container-fluid main-nav clearfix">
          <div class="nav-collapse">
            <ul class="nav">
              <li>
                <a class="current" href="#"><span aria-hidden="true" class="fa fa-home"></span>Inicio</a>
              </li>
              <li><a href="{!! URL::to('combos') !!}">
                <span aria-hidden="true" class="fa fa-dropbox"></span>Combos</a>
              </li>
              <li class="dropdown"><a data-toggle="dropdown" href="index.html#">
                <span aria-hidden="true" class="fa fa-glass"></span>Productos<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="{!! URL::to('bebidas') !!}">Bebidas</a>
                  </li>
                  <li>
                    <a href="{!! URL::to('cigarros') !!}">Cigarrillos</a>
                  </li>
                  <li>
                    <a href="{!! URL::to('extras') !!}">Productos extras</a>
                  </li>
                </ul>
              </li>
              <li><a href="{!! URL::to('proveedores') !!}">
                <span aria-hidden="true" class="fa fa-truck"></span>Proveedores</a>
              </li>
              <li class="dropdown"><a data-toggle="dropdown" href="index.html#">
                <span aria-hidden="true" class="hightop-tables"></span>Inventario<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="#">Bebidas</a>
                  </li>
                  <li>
                    <a href="{!! URL::to('cigarros') !!}">Cigarrillos</a>
                  </li>
                  <li>
                    <a href="#">Productos extra</a>
                  </li>
                </ul>
              </li>
              <li><a href="#">
                <span aria-hidden="true" class="fa fa-users"></span>Usuarios</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <!-- End Navigation -->
      <div class="container-fluid main-content">
        <div class="row">
          <div class="col-lg-12">
            @yield('content')
          </div>
        </div>
      </div>
    </div>
  </body>
  @yield('scripts')
</html>