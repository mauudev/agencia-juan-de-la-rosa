@extends('agencia.principal')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="widget-container fluid-height clearfix">
      <div class="heading">
        <i class="fa fa-table"></i>Lista de proveedores
        <a class="btn btn-primary ladda-button progress-demo pull-right" data-style="expand-right" href="{!! URL::to('proveedores/create') !!}">Agregar nuevo</a>
        &nbsp;<a class="btn btn-info pull-right" data-style="expand-right" href="{!! URL::to('proveedores') !!}"><i class="fa fa-refresh"></i>Actualizar</a>
      </div>
      <div class="widget-content padded clearfix">
        <div id="dataTable1_wrapper" class="dataTables_wrapper" role="grid">
          <div class="dataTables_filter" id="dataTable1_filter">
            <label>Buscar:
              <input type="text" aria-controls="dataTable1">
            </label>         
          </div>
          <table class="table table-bordered table-striped dataTable" aria-describedby="dataTable1_info">
            <thead>
              <tr role="row">
                <th class="sorting_asc" role="columnheader" tabindex="0" rowspan="1" colspan="1"  style="width: 183px;">
                  Nombre proveedor
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Tel&eacute;fono proveedor
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Nombre empresa
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Tel&eacute;fono empresa
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Fecha de registro
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 50px;">
                  Acci&oacute;n
                </th>
              </tr>
            </thead>
            <tbody role="alert" aria-live="polite" aria-relevant="all">
              @foreach($proveedores as $prov)
              <tr class="odd">
                <td class=" sorting_1">
                  {{ $prov->nombre_proveedor }}
                </td>
                <td class="">
                  {{ $prov->telefono_proveedor }}
                </td>
                <td class="hidden-xs">
                  {{ $prov->nombre_empresa }}
                </td>
                <td class="hidden-xs">
                  {{ $prov->telefono_empresa }}
                </td>
                <td class="hidden-xs">
                  {{ $prov->created_at }}
                </td>
                <td class="actions ">
                  <div class="action-buttons">
                    <a class="table-actions" data-toggle="modal" href="#myModal{{ $prov->id }}"><i class="fa fa-eye"></i></a>
                    <a class="table-actions" href="{!! route('proveedores.edit',$parameters = $prov->id) !!}">
                      <i class="fa fa-pencil"></i>
                    </a>
                    <a class="table-actions" href="{!! route('proveedores.destroy',$parameters = $prov->id) !!}">
                      <i class="fa fa-trash-o"></i>
                    </a>
                  </div>
                  <div class="modal fade" id="myModal{{ $prov->id }}" aria-hidden="true" style="display: none;">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                        <h4 class="modal-title">
                          Detalles
                        </h4>
                      </div>
                      <div class="modal-body">
                        <h1>
                          Detalles del proveedor
                        </h1>
                        <p><b>Nombre: </b></p>
                        <p>{{ $prov->nombre_proveedor }}</p>
                        <p><b>T&eacute;lefono proveedor: </b></p>
                        <p>{{ $prov->telefono_proveedor }}</p>
                        <p><b>Nombre de la empresa: </b></p>
                        <p>{{ $prov->nombre_empresa }}</p>
                        <p><b>T&eacute;lefono de la empresa: </b></p>
                        <p>{{ $prov->telefono_empresa }}</p>
                        <p><b>Fecha de creaci&oacute;n: </b></p>
                        <p>{{ $prov->created_at }}</p>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-default-outline" data-dismiss="modal" type="button">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          {{ $proveedores->render() }}
          @include('alerts.success')
        </div>
      </div>
    </div>
  </div>
  @endsection