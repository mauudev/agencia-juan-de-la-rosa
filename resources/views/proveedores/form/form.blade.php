<div class="form-group">
	{!! Form::label('nombre_proveedor','Nombre del proveedor: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!! Form::text('nombre_proveedor',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del proveedor','required','min'=>5]) !!}<br/>
	</div>
</div>
<div class="form-group">
	{!! Form::label('telefono_proveedor','Telefono del proveedor: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!! Form::text('telefono_proveedor',null,['class'=>'form-control','placeholder'=>'Ingrese el telefono del proveedor','required','min'=>7]) !!}<br/>
	</div>
</div>
<div class="form-group">
	{!! Form::label('nombre_empresa','Nombre de la empresa: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!! Form::text('nombre_empresa',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre de la empresa','required','min'=>5]) !!}<br/>
	</div>
</div>
<div class="form-group">
	{!! Form::label('telefono_empresa','Telefono de la empresa: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!! Form::text('telefono_empresa',null,['class'=>'form-control','placeholder'=>'Ingrese el telefono de la empresa','required','min'=>7]) !!}<br/>
	</div>
</div>
