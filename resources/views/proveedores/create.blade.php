@extends('agencia.principal')
@section('content')
<!-- Statistics -->
{{--         <div class="row">
          <div class="col-lg-12">
            <div class="widget-container stats-container">
              <div class="col-md-3">
                <div class="number">
                  <div class="icon globe"></div>
                  86<small>%</small>
                </div>
                <div class="text">
                  Overall growth
                </div>
              </div>
              <div class="col-md-3">
                <div class="number">
                  <div class="icon visitors"></div>
                  613
                </div>
                <div class="text">
                  Unique visitors
                </div>
              </div>
              <div class="col-md-3">
                <div class="number">
                  <div class="icon money"></div>
                  <small>$</small>924
                </div>
                <div class="text">
                  Sales
                </div>
              </div>
              <div class="col-md-3">
                <div class="number">
                  <div class="icon chat-bubbles"></div>
                  325
                </div>
                <div class="text">
                  New followers
                </div>
              </div>
            </div>
          </div>
        </div> --}}
        <!-- End Statistics -->
<div class="row">
	<div class="col-lg-12">
		<div class="widget-container fluid-height clearfix">
			<div class="heading">
				<i class="fa fa-bars"></i>Registrar nuevo proveedor
			</div>
			<div class="widget-content padded">
				{!! Form::open(['route'=>'proveedores.store','method'=>'POST','class'=>'form-horizontal']) !!}
				@include('proveedores.form.form')
				<div class="form-group">
					<div class="col-md-7" >
						{!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!} <a class="btn btn-default-outline" href="{!! URL::to('proveedores') !!}">Cancelar</a>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection