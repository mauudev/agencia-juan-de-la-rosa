@extends('agencia.principal')
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="widget-container fluid-height clearfix">
			<div class="heading">
				<i class="fa fa-bars"></i>Registrar nueva categor&iacute;a
			</div>
			<div class="widget-content padded">
				{!! Form::open(['route'=>'categorias.store','method'=>'POST','class'=>'form-horizontal']) !!}
				@include('categorias.form.form')
				<div class="form-group">
					<div class="col-md-7" >
						{!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!} <a class="btn btn-default-outline" href="{!! URL::to('categorias') !!}">Cancelar</a>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection
{{--@section('scripts')
  {!! Html::script('js/jquery-3.0.0.min.js') !!}
  {!! Html::script('js/dropdown.js') !!}
@endsection  --}}
