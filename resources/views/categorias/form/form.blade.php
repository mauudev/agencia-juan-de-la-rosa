<div class="form-group">
	{!! Form::label('categoria','Categoria del producto: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!! Form::text('categoria',null,['class'=>'form-control','placeholder'=>'Ingrese una categoria','required','min'=>5]) !!}<br/>
	</div>
</div>
