@extends('agencia.principal')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="widget-container fluid-height clearfix">
      <div class="heading">
        <i class="fa fa-table"></i>Lista de productos extra
        <a class="btn btn-primary pull-right" data-style="expand-right" href="{!! URL::to('extras/create') !!}"><i class="fa fa-plus"></i>Agregar nuevo</a>
        &nbsp;<a class="btn btn-info pull-right" data-style="expand-right" href="{!! URL::to('extras') !!}"><i class="fa fa-refresh"></i>Actualizar</a>
      </div>
      <div class="widget-content padded clearfix">
        <div id="dataTable1_wrapper" class="dataTables_wrapper" role="grid">
          <div class="dataTables_filter" id="dataTable1_filter">
            <label>Buscar:
              <input type="text" aria-controls="dataTable1">
            </label>         
          </div>
          <table class="table table-bordered table-striped dataTable" aria-describedby="dataTable1_info">
            <thead>
              <tr role="row">
                <th class="sorting_asc" role="columnheader" tabindex="0" rowspan="1" colspan="1"  style="width: 183px;">
                  Nombre producto
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Precio compra
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Precio venta
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Ganancia
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Proveedor
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Categor&iacute;a
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Fecha de creaci&oacute;n
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 50px;">
                  Acci&oacute;n
                </th>
              </tr>
            </thead>
            <tbody role="alert" aria-live="polite" aria-relevant="all">
              @foreach($extras as $extra)
              <tr class="odd">
                <td class=" sorting_1">
                  {{ $extra->nombre }}
                </td>
                <td class="">
                  {{ $extra->precio_compra }}
                </td>
                <td class="hidden-xs">
                  {{ $extra->precio_venta }}
                </td>
                <td class="hidden-xs">
                  {{ $extra->ganancia }}
                </td>
                <td class="hidden-xs">
                  {{ $extra->nombre_proveedor }}
                </td>
                <td class="hidden-xs">
                  {{ $extra->categoria }}
                </td>
                <td class="hidden-xs">
                  {{ $extra->created_at }}
                </td>
                <td class="actions ">
                  <div class="action-buttons" align="center">
                    <a class="table-actions" data-toggle="modal" href="#myModal{{ $extra->id }}"><i class="fa fa-eye"></i></a>
                    <a class="table-actions" href="{!! route('extras.edit',$parameters = $extra->id) !!}">
                      <i class="fa fa-pencil"></i>
                    </a>
                    <a class="table-actions" href="{!! route('extras.destroy',$parameters = $extra->id) !!}">
                      <i class="fa fa-trash-o"></i>
                    </a>
                  </div>
                  <div class="modal fade" id="myModal{{ $extra->id }}" aria-hidden="true" style="display: none;">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                        <h4 class="modal-title">
                          Detalles
                        </h4>
                      </div>
                      <div class="modal-body">
                        <h1>
                          Detalles del producto
                        </h1>
                        <p><b>Nombre: </b></p>
                        <p>{{ $extra->nombre }}</p>
                        <p><b>Precio de compra: </b></p>
                        <p>{{ $extra->precio_compra }}</p>
                        <p><b>Precio de venta </b></p>
                        <p>{{ $extra->precio_venta }}</p>
                        <p><b>Proveedor: </b></p>
                        <p>{{ $extra->nombre_proveedor }}</p>
                        <p><b>Categor&iacute;a: </b></p>
                        <p>{{ $extra->categoria }}</p>
                        <p><b>Fecha de creaci&oacute;n: </b></p>
                        <p>{{ $extra->created_at }}</p>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-default-outline" data-dismiss="modal" type="button">Cerrar</button>
                      </div>
                    </div>
                  </div>
                </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          {{ $extras->render() }}
          @include('alerts.success')
        </div>
      </div>
    </div>
  </div>
  @endsection