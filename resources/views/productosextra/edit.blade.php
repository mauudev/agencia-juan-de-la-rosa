@extends('agencia.principal')
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="widget-container fluid-height clearfix">
			<div class="heading">
				<i class="fa fa-bars"></i>Actualizar datos del producto extra
			</div>
			<div class="widget-content padded">
				{!!Form::model($extra,['route'=>['extras.update',$extra->id],'method'=>'PUT','class'=>'form-horizontal'])!!}
				@include('productosextra.form.form')
				<div class="form-group">
					<div class="col-md-7" >
						{!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!} <a class="btn btn-default-outline" href="{!! URL::to('extras') !!}">Cancel</a>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection