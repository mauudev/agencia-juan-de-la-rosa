@extends('agencia.principal')
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="widget-container fluid-height clearfix">
			<div class="heading">
				<i class="fa fa-bars"></i>Actualizar datos del producto
			</div>
			<div class="widget-content padded">
				{!!Form::model($bebida,['route'=>['bebidas.update',$bebida->id],'method'=>'PUT','class'=>'form-horizontal'])!!}
				@include('bebidas.form.form')
				<div class="form-group">
					<div class="col-md-7" >
						{!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!} <a class="btn btn-default-outline" href="{!! URL::to('bebidas') !!}">Cancel</a>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection