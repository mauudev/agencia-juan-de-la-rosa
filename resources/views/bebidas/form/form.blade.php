<div class="form-group">
	{!! Form::label('nombre','Nombre del producto: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!! Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del producto','required','min'=>5]) !!}<br/>
	</div>
</div>
<div class="form-group">
	{!! Form::label('precio_compra','Precio de compra: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!! Form::text('precio_compra',null,['class'=>'form-control','placeholder'=>'Ingrese el telefono del proveedor','required','min'=>7]) !!}<br/>
	</div>
</div>
<div class="form-group">
	{!! Form::label('precio_venta','Precio de venta: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!! Form::text('precio_venta',null,['class'=>'form-control','placeholder'=>'Ingrese el precio de venta','required','min'=>5]) !!}<br/>
	</div>
</div>
<div class="form-group">
	{!! Form::label('proveedor','Proveedor: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!!Form::select('proveedores_id', $proveedores,null,['id'=>'select1','class'=>'form-control','placeholder'=>'Seleccione un proveedor..','required'])!!}<br>
	</div>
</div>
<div class="form-group">
	{!! Form::label('tipos_bebida_id','Tipo de bebida: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!!Form::select('tipos_bebida_id', $tipos,null,['id'=>'select1','class'=>'form-control','placeholder'=>'Seleccione un tipo de bebida..','required'])!!}<br>
	</div>
</div>