<div class="form-group">
	{!! Form::label('marca','Marca del producto: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!! Form::text('marca',null,['class'=>'form-control','placeholder'=>'Ingrese la marca del producto','required','min'=>5]) !!}<br/>
	</div>
</div>
<div class="form-group">
	{!! Form::label('precio_compra','Precio de compra: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!! Form::text('precio_compra',null,['class'=>'form-control','placeholder'=>'Ingrese el telefono del proveedor','required','min'=>7]) !!}<br/>
	</div>
</div>
<div class="form-group">
	{!! Form::label('precio_venta_caja','Precio de venta por cajetilla: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!! Form::text('precio_venta_caja',null,['class'=>'form-control','placeholder'=>'Ingrese el precio de venta por cajetilla','required','min'=>5]) !!}<br/>
	</div>
</div>
<div class="form-group">
	{!! Form::label('precio_venta_unidad','Precio de venta por unidad: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!! Form::text('precio_venta_unidad',null,['class'=>'form-control','placeholder'=>'Ingrese el precio de venta por unidad','required','min'=>5]) !!}<br/>
	</div>
</div>
<div class="form-group">
	{!! Form::label('proveedor','Proveedor: ',['class'=>'control-label col-md-2']) !!}
	<div class="col-md-7" >
		{!!Form::select('proveedores_id', $proveedores,null,['id'=>'select1','class'=>'form-control','placeholder'=>'Seleccione un proveedor..','required'])!!}<br>
	</div>
</div>