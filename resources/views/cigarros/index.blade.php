@extends('agencia.principal')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="widget-container fluid-height clearfix">
      <div class="heading">
        <i class="fa fa-table"></i>Lista de cigarrillos
        <a class="btn btn-primary pull-right" data-style="expand-right" href="{!! URL::to('cigarros/create') !!}"><i class="fa fa-plus"></i>Agregar nuevo</a>
        &nbsp;<a class="btn btn-info pull-right" data-style="expand-right" href="{!! URL::to('cigarros') !!}"><i class="fa fa-refresh"></i>Actualizar</a>
      </div>
      <div class="widget-content padded clearfix">
        <div id="dataTable1_wrapper" class="dataTables_wrapper" role="grid">
          <div class="dataTables_filter" id="dataTable1_filter">
            <label>Buscar:
              <input type="text" aria-controls="dataTable1">
            </label>         
          </div>
          <table class="table table-bordered table-striped dataTable" aria-describedby="dataTable1_info">
            <thead>
              <tr role="row">
                <th class="sorting_asc" role="columnheader" tabindex="0" rowspan="1" colspan="1"  style="width: 183px;">
                  Marca del cigarrillo
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" rowspan="1" colspan="1"  style="width: 183px;">
                  Precio compra
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Precio venta por cajetilla
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Precio venta por unidad
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 183px;">
                  Proveedor
                </th>
                <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1" style="width: 50px;">
                  Acci&oacute;n
                </th>
              </tr>
            </thead>
            <tbody role="alert" aria-live="polite" aria-relevant="all">
              @foreach($cigarros as $cigarro)
              <tr class="odd">
              <td class=" sorting_1">
                  {{ $cigarro->marca }}
                </td>
                <td class=" sorting_1">
                  {{ $cigarro->precio_compra }}
                </td>
                <td class="hidden-xs">
                  {{ $cigarro->precio_venta_caja }}
                </td>
                <td class="hidden-xs">
                  {{ $cigarro->precio_venta_unidad }}
                </td>
                <td class="hidden-xs">
                  {{ $cigarro->nombre_proveedor }}
                </td>
                <td class="actions ">
                  <div class="action-buttons" align="center">
                    <a class="table-actions" data-toggle="modal" href="#myModal{{ $cigarro->id }}"><i class="fa fa-eye"></i></a>
                    <a class="table-actions" href="{!! route('cigarros.edit',$parameters = $cigarro->id) !!}">
                      <i class="fa fa-pencil"></i>
                    </a>
                    <a class="table-actions" href="{!! route('cigarros.destroy',$parameters = $cigarro->id) !!}" onclick="return confirm('Esta seguro de eliminar? Esta opci&oacute;n no se puede deshacer !')">
                      <i class="fa fa-trash-o"></i>
                    </a>
                  </div>
                  <div class="modal fade" id="myModal{{ $cigarro->id }}" aria-hidden="true" style="display: none;">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
                        <h4 class="modal-title">
                          Detalles
                        </h4>
                      </div>
                      <div class="modal-body">
                        <h1>
                          Detalles del producto
                        </h1>
                        <p><b>Marca: </b></p>
                        <p>{{ $cigarro->marca }}</p>
                        <p><b>Precio de compra: </b></p>
                        <p>{{ $cigarro->precio_compra }}</p>
                        <p><b>Precio venta por cajetilla: </b></p>
                        <p>{{ $cigarro->precio_venta_caja }}</p>
                        <p><b>Precio venta por unidad: </b></p>
                        <p>{{ $cigarro->precio_venta_unidad }}</p>
                        <p><b>Proveedor: </b></p>
                        <p>{{ $cigarro->nombre_proveedor }}</p>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-default-outline" data-dismiss="modal" type="button">Cerrar</button>
                      </div>
                    </div>
                  </div>
                </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          {{ $cigarros->render() }}
          @include('alerts.success')
        </div>
      </div>
    </div>
  </div>
  @endsection